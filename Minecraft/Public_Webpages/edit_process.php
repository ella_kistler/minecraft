<?php
session_start();
 
/* Keep track if there is an error or not */
$error=FALSE;
 
/* Validation of first name */
if(empty($_REQUEST['id'])) {
    /* There was no first name, that's odd */
    $error=TRUE;
} else {
    /* Get the first name from the request */
    $form['id'] = $_REQUEST['id'];
    /* See if the first name matches our not-very-good filter */
    if (!preg_match("/^[0-9]{1,25}$/", $form['id'])) {
        /* No match, display an error */
        $error=TRUE;
    }
}
 
/* If there was no error, load the record.  */
if(!$error) {
    /* Set up the database connection */
    include ("../Resources/db_setup.php");
    $connection = mysqli_connect($server, $username, $password, $database) or die("Unable to connect");
 
    /* DANGER DANGER DANGER
     * This example code is VERY DANGEROUS
     * because it is missing an important items.
     *
     * 1) If there was a log-in, this code does not
     * check to see if the person logged in has permission to edit
     * this particular record. Remember that the id value can be
     * changed and anyone could swap it for a different number.
     */

    /* Escape the string to prevent SQL injection */
    //$id_safe = mysql_real_escape_string($form['id']);
    $id_safe = mysqli_real_escape_string($connection,$form['id']);
 
    /* Construct the SQL statement */
    $query="select * from questionnaire where id = $id_safe";
 
    /* Run the SQL statement */
    $result = mysqli_query($connection, $query) or die("Query failed");
 
    $row = mysqli_fetch_assoc($result);
    
    
    if($row) {
        /* Grab the data and put in the form array */
        $form['id']=$row['id'];
        $form['firstname']=$row['firstname'];
        $form['lastname']=$row['lastname'];
        $form['gender']=$row['gender'];
        $form['startdate']=$row['startdate'];
        $form['mode']=$row['favmode'];
        $form['transportation']['walking']=$row['trans_walking'];
        $form['transportation']['sprinting']=$row['trans_sprinting'];
        $form['transportation']['pig']=$row['trans_pig'];
        $form['transportation']['horse']=$row['trans_horse'];
        $form['transportation']['minecart']=$row['trans_minecart'];
        $form['transportation']['boat']=$row['trans_boat'];
        $form['transportation']['other']=$row['trans_other'];
        $form['quantity']=$row['mostdiamonds'];

    } else {
        // We didn't find the row the user was looking for.
        die("That record doesn't exist.");
    }
 
    /* Save form to session */
    $_SESSION['form'] = $form;
 
    /* Send message to user */
    $messages['message']="Editing the record for " . $form['firstname'] . " " . $form['lastname'];
 
    /* Save messages to session */
    $_SESSION['messages'] = $messages;
 
    /* Go to the form */
    header("Location: questionnaire.php");
} else {
    /* Error, just go back */
    header("Location: show_database_stuff.php");
}
?>