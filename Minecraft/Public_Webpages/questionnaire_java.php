<!DOCTYPE html>
<html lang="en">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Add to Database Form</title>
  <link rel="stylesheet" type="text/css" href="CSS/forms.css">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>
<body>

<div> 
<h1>Fill Me Out</h1>


<?php
  session_start();
   if(isset($_SESSION['messages'])) {
    $messages = $_SESSION['messages'];
    $form = $_SESSION['form'];
    unset($_SESSION['messages']);
    unset($_SESSION['form']);
  }


  if(!empty($messages)) {
          foreach ($messages as $message) {
              <div class="alert alert-danger" role="alert">$message</div>
          }
  }
?>



<div>
<form action="questionnaire_process.php" method="post" autocomplete="off">

<?php
    if (isset($form['id'])) {
    $id_safe = htmlentities($form['id']);
    echo "<input type='hidden' name='id' value='$id_safe'>";
    }
?>

<label for="firstname"> First Name: </label>
<input id="firstname" name="firstname" type="text" placeholder="name"<?php
    if( isset($form['firstname']) ) {
        $firstname_safe = htmlentities ($form['firstname']);
        echo "value='$firstname_safe'";
    }
    ?>/>
<br />

<label for="lastname"> Last Name: </label>
<input id="lastname" name="lastname" type="text" placeholder="last name here"<?php
    if( isset($form['lastname']) ) {
        $lastname_safe = htmlentities ($form['lastname']);
        echo "value='$lastname_safe'";
    }
    ?>/>
<br />

<p>Gender</p>
<fieldset>
	<input type="radio" name="gender" value="Male" <?php if( isset($form['gender']) and $form['gender']=="Male" ) {echo "checked ";} ?> />
    Male<br />
  <input type="radio" name="gender" value="Female" <?php if( isset($form['gender']) and $form['gender']=="Female" ) {echo "checked ";} ?> />
    Female<br />
	<input type="radio" name="gender" value="Unspecified" <?php if( isset($form['gender']) and $form['gender']=="Unspecified" ) {echo "checked ";} ?> />
    Unspecified 
</fieldset> <br />

 <br />

     <!-- Select a date -->
    <label for="startdate">I've been playing Minecraft since :</label>
    <input type="date" id="startdate" name="startdate" min="1900-01-01" max="2016-01-01"<?php
    if( isset($form['startdate']) ) {
        $startdate = htmlentities ($form['startdate']);
        echo "value='$startdate'";
    }
    ?>/>
    <br />

<!-- drop-down -->
    <label for="mode">What is your favorite mode to play in?</label>
    <input name="mode" id="mode" list="mode_list"<?php
    if( isset($form['mode']) ) {
        $favmode = htmlentities ($form['mode']);
        echo "value='$favmode'";
    }
    ?>/>
    <datalist id="mode_list">
    <option label="Survival" value="Survival">
    <option label="Creative" value="Creative">
    <option label="Adventure" value="Adventure"> 
    <option label="Hardcore" value="Hardcore">
    <option label="Spectator" value="Spectator">
    </datalist>
    <br />

<p>Favorite mode of transportation in minecraft?</p>
<fieldset>
	Walking <input type="checkbox" name="transportation[]" value="walking"
	 <?php if( isset($form['transportation']['walking']) ) {echo "checked ";} ?> /> <br/>

	Sprinting <input type="checkbox" name="transportation[]" value="sprinting"
	 <?php if( isset($form['transportation']['sprinting']) ) {echo "checked ";} ?> /> <br/>

	Pig <input type="checkbox" name="transportation[]" value="pig"
	 <?php if( isset($form['transportation']['pig']) ) {echo "checked ";} ?> /> <br/>

	Horse <input type="checkbox" name="transportation[]" value="horse"
	 <?php if( isset($form['transportation']['horse']) ) {echo "checked ";} ?> /><br/>

	Minecart <input type="checkbox" name="transportation[]" value="minecart"
	 <?php if( isset($form['transportation']['minecart']) ) {echo "checked ";} ?> /> <br/>

  Boat <input type="checkbox" name="transportation[]" value="boat"
    <?php if( isset($form['transportation']['boat']) ) {echo "checked ";} ?> /> <br/>

  Other <input type="checkbox" name="transportation[]" value="other"
    <?php if( isset($form['transportation']['other']) ) {echo "checked ";} ?> /> <br/>
	
</fieldset>

 
    <!-- Select a quantity -->
   <label for="quantity">What's the most diamonds you've ever collected in survival mode?</label>
    <input id="quantity" type="number" name="quantity"<?php
    if( isset($form['quantity']) ) {
        $quantity = htmlentities ($form['quantity']);
        echo "value='$quantity'";
    }
    ?>/>
<br /> 


<!-- submit button -->
<input type="submit" value="Submit"/>



</form>
</div>
</body>
</html>