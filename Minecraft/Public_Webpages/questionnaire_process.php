<link rel="stylesheet" type="text/css" href="CSS/forms.css">
  <?php
  session_start();
  $error=False;

  if(empty($_REQUEST['id'])) {
    $form['id']="";
} else {
    $form['id'] = $_REQUEST['id'];
    if (!preg_match("/^[0-9]{1,25}$/", $form['id'])) {
        /* No match, what is happening? */
        $error=TRUE;
        /* Create an array of messages to display the user */
        $messages['id']="<p class='errormsg'>System error, unable to edit record.</p>";
    }
}

  // first name 
  if(empty($_REQUEST['firstname'])) {
    $error=TRUE;
    $messages['firstname']="<p class='errormsg'>Error - Empty First Name </p>";
} else {
  $form['firstname'] = $_REQUEST['firstname'];
      if (!preg_match("/^[A-Za-z]{2,25}$/", $form['firstname'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['firstname']="<p class='errormsg'>Error - Invalid First Name</p>"; 
         
    }
}

//last name
  
    if(empty($_REQUEST['lastname'])) {
    $error=TRUE;
    $messages['lastname']="<p class='errormsg'>Error - Empty Last Name</p>";
} else {
  $form['lastname'] = $_REQUEST['lastname'];
      if (!preg_match("/^[A-Za-z]{2,25}$/", $form['lastname'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['lastname']="<p class='errormsg'>Error - Invalid Last Name</p>"; 
         
    }
}
 
  //gender
  if(!empty($_REQUEST['gender'])) {
    $form['gender'] = $_REQUEST['gender'];
    if (!preg_match("/^(Male|Female|Unspecified)$/", $form['gender'] )) {
        $error=TRUE;
        $messages['gender']="<p class='errormsg'>Error - Invalid Gender</p>"; 
    }
} else {
    $error=TRUE;
    $messages['gender']="<p class='errormsg'>Error - No Gender Selected </p>";
}


 //start date
  if( empty($_REQUEST['startdate'])) {
       $error=TRUE;
    $messages['startdate']="<p class='errormsg'>Error - Empty Startdate</p>";
} else {
  $form['startdate'] = $_REQUEST['startdate'];
      if (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $form['startdate'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['startdate']="<p class='errormsg'>Error - Invalid Startdate</p>"; 
         
    }
}

// mode
 if(empty($_REQUEST['mode'])) {
    $error=TRUE;
    $messages['mode']="<p class='errormsg'>Error - No Mode Selected</p>";
} else {
  $form['mode'] = $_REQUEST['mode'];
      if (!preg_match("/^(Spectator|Hardcore|Adventure|Creative|Survival)$/", $form['mode'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['mode']="<p class='errormsg'>Error - Invalid Mode</p>"; 
         
    }
}


if( empty($_REQUEST['transportation'])) {
	$error=TRUE;
    $messages['transportation']="<p class='errormsg'>Error - Empty Transportation</p>"; 
} else {
    $transportation = $_REQUEST['transportation'];
    $form['transportation'] = [];
 
    foreach($transportation as $trans) {
        if (!preg_match("/^(walking|sprinting|pig|horse|minecart|boat|other)$/", $trans)) {
            $error=TRUE;
            $messages['transportation']="Error - Invalid Transportation Selection"; 
        } else {
            $form['transportation'][$trans] = TRUE;
        }
    }
}

if(empty($_REQUEST['quantity']))  {
    $error=TRUE;
    $messages['quantity']="<p class='errormsg'>Error - Empty Quantity</p>";
} else {
  $form['quantity'] = $_REQUEST['quantity'];
      if (!preg_match("/^[0-9]{1,10}$/", $form['quantity'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['quantity']="<p class='errormsg'>Error - Invalid quantity</p>"; 
         
    }
}



if($error==FALSE) {
    /* Set up the database connection */
    include ("../Resources/db_setup.php");
 
    $connection = mysqli_connect($server, $username, $password, $database) or die("Unable to connect");
 
  
    $id_safe = mysqli_escape_string($connection, $form['id']);
    $firstname_safe = mysqli_escape_string($connection, $form['firstname']);
    $lastname_safe = mysqli_escape_string($connection, $form['lastname']);
    $gender = mysqli_escape_string($connection, $form['gender']);
    $startdate = mysqli_escape_string($connection, $form['startdate']);
    $favmode = mysqli_escape_string($connection, $form['mode']);
    
    if(isset($form['transportation']['walking']))  {
     $trans_walking = 1;
    } else {
      $trans_walking = 0;
     }
    
    if(isset($form['transportation']['sprinting']))  {
     $trans_sprinting = 1;
    } else {
      $trans_sprinting = 0;
    }

    if(isset($form['transportation']['pig']))  {
     $trans_pig = 1;
    } else {
      $trans_pig = 0;
    }
      
    if(isset($form['transportation']['horse']))  {
     $trans_horse = 1;
    } else {
      $trans_horse = 0;
    }
      
    if(isset($form['transportation']['minecart']))  {
     $trans_minecart = 1;
    } else {
      $trans_minecart = 0;
      }

    if(isset($form['transportation']['boat']))  {
     $trans_boat = 1;
    } else {
      $trans_boat = 0; 
    }

    if(isset($form['transportation']['other']))  {
     $trans_other = 1;
    } else {
      $trans_other = 0;  
      }   

    $mostdiamonds = mysqli_escape_string($connection, $form['quantity']);
 


    if( $id_safe == "" ) {
    $query="insert into cis305.questionnaire ('id', 'firstname', 'lastname', 'gender', `startdate', 'favmode', 'trans_walking', 'trans_sprinting', 'trans_pig', 'trans_horse', 'trans_minecart', 'trans_boat', 'trans_other', 'mostdiamonds') values (Null, '$firstname_safe', '$lastname_safe', '$gender', '$startdate', '$favmode', '$trans_walking', '$trans_sprinting', '$trans_pig', '$trans_horse', '$trans_minecart', '$trans_boat', '$trans_other', '$mostdiamonds')";
    } else {
      $query="update cis305.questionnaire set id='$id_safe', firstname = '$firstname_safe', lastname='$lastname_safe', gender= '$gender', startdate= '$startdate', favmode ='$favmode', trans_walking='$trans_walking', trans_sprinting= '$trans_sprinting', trans_pig= '$trans_pig', trans_horse= '$trans_horse', trans_minecart ='$trans_minecart', trans_boat ='$trans_boat', trans_other='$trans_other', mostdiamonds='$mostdiamonds' where id='$id_safe';";
      
    }
      echo "$query"; 
      echo "</br>";

     //UPDATE `cis305`.`questionnaire` SET `startdate` = '2011-06-09', `favmode` = 'Spectator', `trans_horse` = '1', `mostdiamonds` = '16' WHERE `questionnaire`.`id` = 3; 
     //$sql = "UPDATE `cis305`.`questionnaire` SET `startdate` = \'2011-06-09\', `favmode` = \'Spectator\', `trans_horse` = \'1\', `mostdiamonds` = \'16\' WHERE `questionnaire`.`id` = 3;"; 
    
    /* Run the SQL statement */
    mysqli_query($connection, $query) or die("Query failed to run.");
    mysqli_close($connection);
     
    header("Location: show_database_stuff.php");
} else {
    $_SESSION['messages'] = $messages;
    $_SESSION['form'] = $form;
    header("Location: questionnaire.php");
}

  ?>