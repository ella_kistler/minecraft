<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title> The Next Page</title>
  <link rel="stylesheet" type="text/css" href="CSS/forms.css">
</head>
<body>
<div> 
<?php
echo "Hello $firstname $lastname. <br />";
echo "You are $gender. <br />";
echo "You have been gaming since $startdate<br />";
echo "You like to play in $mode mode. <br />";

if( isset($_REQUEST['transportation'])) {
   // They did. Get an array of each one
   $transportation = $_REQUEST['transportation'];
   // How many items in the array?
   $n = count($transportation);
   // Print the array
   for($i=0; $i<$n;$i++) {
    echo "You travel by $transportation[$i]. <br />";
   }
  }
  echo "You have $quantity diamonds.<br />";
?>
</div>
</body>
</html>