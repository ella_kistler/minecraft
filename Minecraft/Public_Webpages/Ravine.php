<!doctype html>
<head>
	<meta charset="utf-8" />
	<TITLE>Working with Ravines</TITLE>
	<script type="text/javascript" charset="utf-8" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="CSS/blocks.css">
</head>
<body>
<div class ="primary"> 
	<h1>Working with Ravines</h1>
	
<table class="menu">
      <tr>
        <td>  <img src="Pictures/Other/grass_block.png" alt="grass block" />    </td>
           <td> <?php include('menu_javascript.php'); ?> </td>
          <td>  <img src="Pictures/Other/grass_block.png" alt="grass block" />      </td>
      </tr>
  </table>

	<p>There is some general info in the <a href="http://minecraft.wikia.com/wiki/Ravine" target="_blank">wiki</a>. So you’re walking along in the woods or gallivanting in the plains when suddenly you find yourself on the edge of a cliff of death with a lava floor. Don't despair there is a treasure trove of goodies down there. However you also have a huge safety hazard in front of you. You and your battle burro almost fell 40 blocks to your death. You have three options you could leave and never come back or you could make note of the discovery on your map then cover the top with dirt or you could take advantage of the opening in the open air and cover the top with glass. That way when you do get into the ravine it cuts down on the mobs messing with you when you’re working. </p>
	</div> 
</body>
