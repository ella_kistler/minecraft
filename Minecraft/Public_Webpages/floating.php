<!doctype html>
<head>
  <meta charset="utf-8" />
  <TITLE> Story Time</TITLE>
  <script type="text/javascript" charset="utf-8" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="CSS/blocks.css">
</head>

<body>
<div class ="primary">
<h1>Howl's Floating Castle</h1>

<table class="menu">
      <tr>
        <td>  <img src="Pictures/Other/grass_block.png" alt="grass block" />    </td>
           <td> <?php include('menu_javascript.php'); ?> </td>
          <td>  <img src="Pictures/Other/grass_block.png" alt="grass block" />      </td>
      </tr>
  </table>


</div>
  
<table class="alone">
  <thead>
    <tr>
      <th> </th>
      <th> </th>
     
    </tr>
  </thead>
  <tbody>
    <tr>
      <td> <p>Once there were two princess that got tired of living in the desert. Not so much because of the sand getting everywhere, but because the people living in the desert village had no sense of personal space and constantly entered the princesses’ house without knocking first. <em>Sometimes they would even host slumber parties or town meetings in the princesses house without telling them.</em> </p>
      <p> So the princesses fed up with the ungrateful subjects, packed up their stuff and scouted a cool place to build a floating castle. When they found a spiffy looking hill the older and wiser of the two presented the castle blue prints she made using Microsoft Paint. </p>
      <p>The following is a list of things to consider if any wants to also make a floating castle.</p>
          <ul>
      <li>
        Gravity sucks.
      </li>
 
      <li>
        You never have enough stone. True for any castle project.
      </li>
 
      <li>
        Build good defenses for arrows.
      </li>
    </ul></td>

      <td><img src="Pictures/Content/BluePrints.png" alt="Blue Prints" /></td>
     
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <td>  </td>
      <td>Best blue print ever.</td>
     
    </tr>
  </tfoot>
</table>


</body>
</html>
