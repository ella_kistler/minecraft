<!doctype html>
<head>
	<meta charset="utf-8" />
	<TITLE>New Players</TITLE>
  <script type="text/javascript" charset="utf-8" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="CSS/blocks.css">
</head>
<body>
<div class ="primary"> 
<h1>Minecraft Boot Camp</h1>

<table class="menu">
      <tr>
        <td>  <img src="Pictures/Other/grass_block.png" alt="grass block" />    </td>
           <td> <?php include('menu_javascript.php'); ?> </td>
          <td>  <img src="Pictures/Other/grass_block.png" alt="grass block" />      </td>
      </tr>
  </table>

<p>When introducing a friend to the game you need to start with explaining Minecraft logic. It saves a lot of questions. <em> Why hasn’t that structure crumbled yet? Don’t you need support beams to hold up the dirt when you’re tunneling? How is that castle floating? </em> It’s Minecraft. The laws of physics are optional. The best way to demonstrate this is have them cut down a tree have them call out ‘timber’ and watch as the tree is anticlimactically still standing.</p>
<p>It would also save alot of time to have them get the hang of the controls in peaceful. There are no monsters to deal with and some poeple have troubles remembering to eat when they are first starting out.</p>
<p>Be patient with them. If they think they are set with a dirt house,'stove', bed, and crafting table you let them. Eventualy they will realize that the house is to small then move on to a bigger one like a hermit crab. And watching the new play learn the hard way that you can't swim in lava makes the game more fun.</p>

	</div>
</body>
