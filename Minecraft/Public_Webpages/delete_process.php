<?php
session_start();
 
/* Keep track if there is an error or not */
$error = FALSE;
 
/* Validation of first name */
if(empty($_REQUEST['id'])) {
    /* There was no first name, that's odd */
    $error = TRUE;
} else {
    /* Get the first name from the request */
    $form['id'] = $_REQUEST['id'];
    /* See if the first name matches our not-very-good filter */
    if (!preg_match("/^[0-9]{1,25}$/", $form['id'])) {
        /* No match, display an error */
        $error = TRUE;
    }
}
 
/* If there was no error, delete the record.  */
if(!$error ) {
    /* Set up the database connection */
    include ("../Resources/db_setup.php");
    $connection = mysqli_connect($server, $username, $password, $database) or die("Unable to connect");
    /* DANGER DANGER DANGER
     * This example code is VERY DANGEROUS
     * because it is missing two items.
     *
     * 1) In this example, the user does not need to be logged in
     * to get to this point. Anyone may delete the people in the
     * database. If a search engine explores the web site, it will
     * explore all the delete links and delete all the records in
     * the database.
     *
     * 2) Even if there was a log-in, this code does not
     * check to see if the person logged in has permission to delete
     * this particular record. Remember that the id value can be
     * changed and anyone could swap it for a different number.
     */
    /* Escape the string to prevent SQL injection */
    $id_safe = mysqli_escape_string($connection, $form['id']);
 
    /* Construct the SQL statement */
    $query = "delete from cis305.questionnaire where questionnaire.id = $id_safe"; 
     
 
    /* Run the SQL statement */
    mysqli_query($connection, $query);
    mysqli_close($connection);
}
/* Go back to the list people page. */
header("Location: show_database_stuff.php");
 
?>