<!DOCTYPE html>
  
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  
  <title>List People</title>
</head>
  
<body>
  <h1>List of people:</h1>
  <?php
  /* Info to hook up to a database.
   * Normally, we'd import this from a common file. You would
   * not want to update this in each file when the password
   * changes.
   */
  $username = "cis305";
  $password = "W@ffle";
  $database = "cis305";
  $server   = "127.0.0.1";
  
  $connection = mysqli_connect($server, $username, $password, $database) or die("Unable to connect");
 
  // Pull all users from the database
  $query = "SELECT * FROM people";
  $result = mysqli_query($connection, $query) or die("Query failed");
  
  // Loop through each record. This could also
  // be done with a 'for' loop.
  while($row = mysqli_fetch_assoc($result)) {
    $firstname = $row['first'];
    $lastname = $row['last'];
    echo "$firstname $lastname<br />";
  }
  mysqli_free_result($result);
  mysqli_close($connection);
  ?>
</body>
</html>