<link rel="stylesheet" type="text/css" href="CSS/forms.css">
  <?php
  session_start();
  $error=False;

  // first name 
  if(empty($_REQUEST['firstname'])) {
    $error=TRUE;
    $messages['firstname']="<p class='errormsg'>Error - Empty First Name </p>";
} else {
  $form['firstname'] = $_REQUEST['firstname'];
      if (!preg_match("/^[A-Za-z]{2,25}$/", $form['firstname'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['firstname']="<p class='errormsg'>Error - Invalid First Name</p>"; 
         
    }
}

//last name
  
    if(empty($_REQUEST['lastname'])) {
    $error=TRUE;
    $messages['lastname']="<p class='errormsg'>Error - Empty Last Name</p>";
} else {
  $form['lastname'] = $_REQUEST['lastname'];
      if (!preg_match("/^[A-Za-z]{2,25}$/", $form['lastname'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['lastname']="<p class='errormsg'>Error - Invalid Last Name</p>"; 
         
    }
}
 
  //gender
  if(!empty($_REQUEST['gender'])) {
    $form['gender'] = $_REQUEST['gender'];
    if (!preg_match("/^(Male|Female|Unspecified)$/", $form['gender'] )) {
        $error=TRUE;
        $messages['gender']="<p class='errormsg'>Error - Invalid gender</p>"; 
    }
} else {
    $error=TRUE;
    $messages['gender']="<p class='errormsg'>Error - No gender selected </p>";
}


 //start date
  if( empty($_REQUEST['startdate'])) {
       $error=TRUE;
    $messages['startdate']="<p class='errormsg'>Error - Empty startdate</p>";
} else {
  $form['startdate'] = $_REQUEST['startdate'];
      if (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $form['startdate'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['startdate']="<p class='errormsg'>Error - Invalid startdate</p>"; 
         
    }
}

// mode
 if(empty($_REQUEST['mode'])) {
    $error=TRUE;
    $messages['mode']="<p class='errormsg'>Error - No Mode Selected</p>";
} else {
  $form['mode'] = $_REQUEST['mode'];
      if (!preg_match("/^(Spectator|Hardcore|Adventure|Creative|Survival)$/", $form['mode'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['mode']="<p class='errormsg'>Error - Invalid Mode</p>"; 
         
    }
}

  
//$transportation = $_REQUEST['transportation'];
if( isset($_REQUEST['transportation'])) {
 // They did. Get an array of each one
 $form['transportation'] = $_REQUEST['transportation'];
 // How many items in the array?
 $n = count($form['transportation']);
 // Print the array
 for($i=0; $i<$n;$i++) {
    if (!preg_match("/^(walking|sprinting|pig|horse|minecart|boat|other)$/", $form['transportation'][$i])) {
        $error=TRUE;
        $messages['transportation']="<p class='errormsg'> Error - Invalid Transportation Selection </p>"; 
    } else {
        $vehicles_associative[$form['transportation'][$i]] = TRUE;
    }
 }
}


if(empty($_REQUEST['quantity']))  {
    $error=TRUE;
    $messages['quantity']="<p class='errormsg'>Error - Empty quantity</p>";
} else {
  $form['quantity'] = $_REQUEST['quantity'];
      if (!preg_match("/^[0-9]{1,10}$/", $form['quantity'])) {
        /* No match, display an error */
        $error=TRUE;
            /* Create an array of messages to display the user */
        $messages['quantity']="<p class='errormsg'>Error - Invalid quantity</p>"; 
         
    }
}



if($error==FALSE) {
    /* Set up the database connection */
    include ("db_setup.php");
 
    $connection = mysqli_connect($server, $username, $password, $database) or die("Unable to connect");
 
    /* transportation[] */
    $firstname_safe = mysqli_escape_string($connection, $form['firstname']);
    $lastname_safe = mysqli_escape_string($connection, $form['lastname']);
    $gender = mysqli_escape_string($connection, $form['gender']);
    $startdate = mysqli_escape_string($connection, $form['startdate']);
    $favmode = mysqli_escape_string($connection, $form['mode']);
    
    if(isset($form['transportation']['walking']))  {
     $trans_walking = 1;
    } else {
      $trans_walking = 0;
     }
    
    if(isset($form['transportation']['sprinting']))  {
     $trans_sprinting = 1;
    } else {
      $trans_sprinting = 0;
    }

    if(isset($form['transportation']['pig']))  {
     $trans_pig = 1;
    } else {
      $trans_pig = 0;
    }
      
    if(isset($form['transportation']['horse']))  {
     $trans_horse = 1;
    } else {
      $trans_horse = 0;
    }
      
    if(isset($form['transportation']['minecart']))  {
     $trans_minecart = 1;
    } else {
      $trans_minecart = 0;
      }

    if(isset($form['transportation']['boat']))  {
     $trans_boat = 1;
    } else {
      $trans_boat = 0; 
    }

    if(isset($form['transportation']['other']))  {
     $trans_other = 1;
    } else {
      $trans_other = 0;  
      }   
    //$trans_walking = mysqli_escape_string($connection, $form['transportation']);
    // $trans_sprinting = mysqli_escape_string($connection, $form['transportation']);
    // $trans_pig = mysqli_escape_string($connection, $form['transportation']);;
    // $trans_horse = mysqli_escape_string($connection, $form['transportation']);
    // $trans_minecart = mysqli_escape_string($connection, $form['transportation']);
    // $trans_boat = mysqli_escape_string($connection, $form['transportation']);
    // $trans_other = mysqli_escape_string($connection, $form['transportation']);
    $mostdiamonds = mysqli_escape_string($connection, $form['quantity']);
 
    /* Construct the SQL statement */
    $query="insert into questionnaire (`id`, `firstname`, `lastname`, `gender`, `startdate`, `favmode`, `trans_walking`, `trans_sprinting`, `trans_pig`, `trans_horse`, `trans_minecart`, `trans_boat`, `trans_other`, `mostdiamonds`) values (Null, '$firstname_safe', '$lastname_safe',$gender,$startdate, $favmode, $trans_walking, $trans_sprinting, $trans_pig, $trans_horse, $trans_minecart, $trans_boat, $trans_other, $mostdiamonds)";
 
    /* Run the SQL statement */
    mysqli_query($connection, $query) or die("Insert query failed to run.");
    mysqli_close($connection);
     
    header("Location: ../Public_Webpages/show_database_stuff.php");
} else {
    $_SESSION['messages'] = $messages;
    $_SESSION['form'] = $form;
    header("Location: ../Public_Webpages/questionnaire.php");
}




  ?>